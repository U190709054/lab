public class TestRectangle {

    public static void main(String[] args) {

        Point p1 = new Point(3,7);

        Rectangle r1 = new Rectangle(5,6,new Point(3,7));

        System.out.println("Area = " + r1.area());

        System.out.println("Perimeter = " + r1.perimeter());

        Point[] corners = r1.corners();
        for (int i = 0; i < corners.length ; i++){


            System.out.println("x = " + corners[i].xCoord + " " +  "y = " + corners[i].yCoord);
        }



    }
}
